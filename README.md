# Paymob Laravel Package

## Installation
1. Install the Paymob Laravel Package via [paymob/laravel-package](https://packagist.org/packages/paymob/laravel-package)

```bash
composer require paymob/laravel-package
```

2. Publish the Paymob Service Provider using the following command.

```bash
php artisan vendor:publish --provider="Paymob\LaravelPackage\PaymobServiceProvider" --tag="paymob"
```

3. Customize the process and callback actions that exist in **app/Http/Controllers/PaymobController.php** file as per your needs.

## Configuration
### Paymob Account
1. Login to the Paymob account → Setting in the left menu. 
2. Get the Secret, public, API keys, HMAC and Payment Methods IDs (integration IDs).

### Merchant Configurations
1. Edit the **config/paymob.php** file and paste each key in its place.
2. Please ensure adding the integration IDs separated by comma ,. These IDs will be shown in the Paymob payment page. 
3. Copy integration callback URL that exists in **config/paymob.php** file, replace only the {YourWebsiteURL} with your site domain. Then, paste it into each payment integration/method in Paymob account.

> https://{YourWebsiteURL}/paymob/callback

4. Below URL is considered as your website payment process for Paymob Payment. Just replace the `{YourWebsiteURL}` with the actual domain.

> https://{YourWebsiteURL}/paymob/process
