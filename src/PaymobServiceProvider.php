<?php

namespace Paymob\LaravelPackage;
use App\Http\Controllers\PaymobController;
use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Route;

class PaymobServiceProvider extends ServiceProvider {

    /**
     * Register services.
     *
     * @return void
     */
    public function register() {
        $this->publishes([
            __DIR__ . '/../config/paymob.php'             => config_path('paymob.php'),
            __DIR__ . '/controllers/PaymobController.php' => app_path() . '/Http/Controllers/PaymobController.php',
                ], 'paymob');

        Route::get('paymob/callback', [
            'as'   => 'paymob.callback', 'uses' => PaymobController::class . '@callback'
        ]);

        Route::get('paymob/process', [
            'as'   => 'paymob.process', 'uses' => PaymobController::class . '@process'
        ]);
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot() {
        $this->mergeConfigFrom(
                __DIR__ . '/../config/paymob.php', 'paymob'
        );
    }
}
